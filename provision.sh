#!/bin/bash

export DEBIAN_FRONTEND=noninteractive

#echo "Adding jessie-backports..."
#echo "deb http://ftp.lt.debian.org/debian/ jessie-backports main" \
#  > /etc/apt/sources.list.d/jessie-backports.list

echo "Updating apt-get..."
apt-get update -qy

echo "Installing openjdk-7-jdk, vim, git, tree..."
apt-get -qy install openjdk-7-jdk vim git tree

echo "Adding JAVA_HOME variable to /etc/profile , and sourcing it..."
echo "export JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64/" >> /etc/profile
source /etc/profile

#echo "Installing maven from jessie-backports..."
#apt-get -qy -t jessie-backports install maven

MAVEN_URL=http://apache.mirror.vu.lt/apache/maven/maven-3/3.3.3/binaries/apache-maven-3.3.3-bin.tar.gz
MAVEN_TEMP=/tmp/mvn333.tar.gz
MAVEN_DIR=/opt/maven-3.3.3

echo "Downloading Maven..."
curl -LsS $MAVEN_URL -o $MAVEN_TEMP

mkdir $MAVEN_DIR

echo "Extracting Maven to $MAVEN_DIR ..."
tar xf $MAVEN_TEMP -C $MAVEN_DIR --strip-components=1

echo "Deleting downloaded archive..."
rm $MAVEN_TEMP

echo "Adding $MAVEN_DIR/bin to PATH in /etc/profile , and sourcing it..."
echo "export PATH=\$PATH:$MAVEN_DIR/bin" >> /etc/profile
source /etc/profile

echo "Setting up 1G swap file..."
dd if=/dev/zero of=/root/swapfile bs=1024 count=1M
chmod 600 /root/swapfile
mkswap /root/swapfile
echo "/root/swapfile swap swap sw 0 0" >> /etc/fstab
swapon /root/swapfile
