(require 'cljs.build.api)

(defn call [api-f] 
  (api-f
    "src/main/clojurescript"
    {:main
     'dmos.raudondvaris.admin
     :optimizations
     :none
     :output-dir
     "${project.build.directory}/${project.build.finalName}/js"
     :output-to
     "${project.build.directory}/${project.build.finalName}/js/main.js"
     :source-map 
     "${project.build.directory}/${project.build.finalName}/js/main.js.map"
     :asset-path
     "/js"
     }))

(defn complain [& ss] (throw (Exception. (apply str ss))))

(defn production []
  (cljs.build.api/build
    "src/main/clojurescript"
    {
    ;:main
    ;'dmos.raudondvaris.admin
     :optimizations
     :simple
     :output-to
     "${project.build.directory}/${project.build.finalName}/js/main.js"
     }))

(case (first *command-line-args*)
  "build" (call cljs.build.api/build)
  "watch" (call cljs.build.api/watch)
  "prod" (production)
  nil
  (complain
    "You should tell me what to do. Valid options are 'watch' or 'build'."))

; #_(cljs.build.api/build
;   "src/main/clojurescript"
;   {
;   ;:main
;   ;'dmos.raudondvaris.admin
;   :optimizations
;   :simple
;   :output-dir
;   "${project.build.directory}/${project.build.finalName}/js"
;   :output-to
;   "${project.build.directory}/${project.build.finalName}/js/main.js"
;   :asset-path
;   "/js"
;   })

(System/exit 0)
