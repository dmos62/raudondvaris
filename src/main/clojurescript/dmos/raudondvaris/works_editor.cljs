(ns dmos.raudondvaris.works-editor
  (:require [om.core :as om :include-macros true]
            [om-tools.dom :as dom :include-macros true]
            [cljs.core.async
             :refer [close! take! chan put! <! >! to-chan]
             :as async]
            [clojure.string :as str]
            [goog.events]
            [goog.events.EventType]
            ;[markdown.core :refer [md->html]]
            [dmos.raudondvaris.trace :refer [TRACE]]
            ;[uzraktas.admin.utils :as utils]
            [dmos.raudondvaris.cursors :as cursors]
            [dmos.raudondvaris.navigation
             :refer [navigate-in-app! match-token internal-link]]
            [dmos.raudondvaris.resource-comms :as rc]
           ;[uzraktas.admin.resource-comms
           ; :refer [map->Resource Editor
           ;         update-local! persist! persist-changes! insert-new!]]
           ;[uzraktas.admin.image-utils
           ; :refer [<input-to-dataurl+file-c put-image!]]
            )
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:import [goog.date UtcDateTime] ; http://docs.closure-library.googlecode.com/git/class_goog_date_UtcDateTime.html
           ))

(def resource
  (rc/map->Resource
    {:cursor (cursors/get-works-cursor)
     :ref-cursor (cursors/get-ref-works-cursor)
     :url "/a/paveikslai"
     }))

(extend-type js/FileList
  ISeqable
  (-seq [fl] (map #(.item fl %) (range (.-length fl)))))

(defn bounding-weights [position works]
  [(:weight (nth works position nil))
   (:weight (nth works (+ position 1) nil))])

(def optimal-gap 100)

(defn optimal-weight [position works]
  (let [[l-w r-w] (bounding-weights position works)]
    (cond
      (nil? l-w) (- (or r-w optimal-gap) optimal-gap)
      (nil? r-w) (+ l-w optimal-gap)
      (<= (- r-w l-w) 1) nil
      true (+ l-w (quot (- r-w l-w) 2))
      )))

(defn reweigh-from [position works]
  (let [[left right] (split-at (+ position 1) works)
        left-shorter (< (count left) (count right))
        shifted 
        (if left-shorter
          (let [ref-w (:weight (nth works (+ position 1)))]
            (->>
              left
              reverse
              (map-indexed
                (fn [i x]
                  (update x :weight #(- ref-w (* (+ i 1) optimal-gap)))))))
          (let [ref-w (:weight (nth works position))]
            (->>
              right
              (map-indexed
                (fn [i x]
                  (update x :weight #(+ ref-w (* (+ i 1) optimal-gap))))))))]
    shifted
    ))

(defn <ikelk-paveiksla! [resource file weight tag]
  (go 
    (->>
      (<! (rc/<upload-images!
            (str (:url resource) "/" tag "/" weight)
            {"image" file}))
      rc/read-body
      (rc/handle-updated! resource))))

(defn kuriam-paveiksla! [file position tag works]
  (let [works (sort-by :weight works)]
    (if-let [weight (optimal-weight position works)]
      (<ikelk-paveiksla! resource file weight tag)
      (let [reweighed (reweigh-from position works)
            changed-keys (set (map :id reweighed))]
        (->> ; trigger persist of reweighed works
             reweighed
             (map #(rc/-persist-single! resource %))
             dorun)
        (->> ; calc new works and recur to persist new paveiksla
             (remove #(contains? changed-keys (:id %)) works)
             (concat reweighed)
             (recur file position tag))))))

(defn single-work [{:keys [paveikslas judinimo-c pakeistas judinamas]} owner]
  (reify
    om/IRender
    (render [_]
      (let [judink
            #(put! judinimo-c {:judinti (:id paveikslas)})
            trink
            #(om/update! paveikslas :delete (not (:delete paveikslas)))]
        (dom/div 
          {:class
           (str "paveikslas"
                (when pakeistas " pakeistas")
                (when judinamas " judinamas")
                (when (:delete paveikslas) " trinamas"))}
          (dom/img
            {:src (:image paveikslas)
             :title (:weight paveikslas)})
          (dom/div
            {:class "irankiai"}
            (dom/a
              {:class "judina" :title "Judinti" :href "#" :on-click judink}
              "j")
            (dom/a
              {:class "trina" :title "Trinti" :href "#" :on-click trink}
              "t"))
          )))))

(defn file-input-position-ref [pos] (str "image-file-input_" pos))

(defn turi-taga [tag]
  (fn [x] (= (:tag x) tag)))

(defn observe-tagged-paintings [owner tag]
  (->>
    (om/observe owner (cursors/get-works-cursor))
    (filter (turi-taga tag))))

(defn space-mark [{:keys [position judinimo-c tag]} owner]
  (reify
    om/IDidMount
    (did-mount [_]
      (goog.events/listen
        (om/get-node owner) goog.events.EventType/CLICK
        #(put! judinimo-c {:i-indeksa position})))
    om/IRender
    (render [_]
      (let
        [works (observe-tagged-paintings owner tag)
         spausk-file-input
         #(.click (om/get-node owner (file-input-position-ref position)))
         naujas-paveikslas
         #(kuriam-paveiksla! (first (.-files (.-target %))) position tag works)]
        (dom/div
          {:class "space-mark"}
          (dom/a
            {:class "naujas" :title "Naujas paveikslas"
             :href "#" :on-click spausk-file-input}
            "n")
          (dom/input
            {:style {:display "none"}
             :type "file"
             :ref (file-input-position-ref position)
             :accept ".jpg,.png"
             ;:on-click #(.stopPropagation %)
             :on-change naujas-paveikslas
             })
          )))))

(defn judinam-paveiksla [judinti-id position works-cursor tag]
  (let [unsorted-works @works-cursor
        works (->> unsorted-works
                  (filter (turi-taga tag))
                  (sort-by :weight)
                  vec)
        ix-in-sorted (rc/index-of-uid works judinti-id)
        ;ismetu judinama paveiksla is svarstomo vektoriaus kad netrugdytu
        works (rc/remove-vector-item works ix-in-sorted)
        ;jeigu pav perkeliamas i desine koreguoju pozicija del ismesto pav
        position
        (cond
          (<= ix-in-sorted position) (- position 1)
          true position)]
    (if-let [weight (optimal-weight position works)]
      ; surask judinti-id paveiksla, ir pakeisk jo weight
      (let [ix-in-ws (rc/index-of-uid unsorted-works judinti-id)]
        (om/update! works-cursor [ix-in-ws :weight] weight))
      ;pakeisk kitu weight
      (let [reweighed (reweigh-from position works)
            changed-keys (set (map :id reweighed))]
        (->>
          (fn [old-ws]
            (->>
              (remove #(contains? changed-keys (:id %)) old-ws)
              (concat reweighed)
              vec))
          (om/transact! works-cursor))
        (recur judinti-id position works-cursor tag)
        ))))

(defn judinimas [c tag owner]
  (go-loop
    []
    (when-let [{:keys [judinti i-indeksa]} (<! c)]
      (cond
        judinti
        (if (= (om/get-state owner :judinti) judinti)
          (om/set-state! owner :judinti nil)
          (om/set-state! owner :judinti judinti))
        i-indeksa
        (when-let [judinti-id (om/get-state owner :judinti)]
          (judinam-paveiksla
            judinti-id i-indeksa (cursors/get-works-cursor) tag)
          (om/set-state! owner :judinti nil)))
      (recur)
      )))

(defn pakeistas? [reference x]
  (let [x (if (:delete x) x (dissoc x :delete))]
    (not (some #{x} reference))))

(defn works-view [tag owner]
  (reify
    om/IInitState
    (init-state [_]
      {:judinimo-c (chan)
       :judinti nil})
    om/IWillMount
    (will-mount [_]
      (judinimas (om/get-state owner :judinimo-c) tag owner))
    om/IRenderState
    (render-state [_ state]
      (let [works
            (->>
              (observe-tagged-paintings owner tag)
              (sort-by :weight))
            ref-works (om/observe owner (cursors/get-ref-works-cursor))
            judinimo-c (:judinimo-c state)
            judinamas (:judinti state)]
        (dom/div 
          {:id "paveikslai"
           :class (when (:judinti state) "judina")}
          (om/build
            space-mark
            {:position -1 :judinimo-c judinimo-c :tag tag})
          (map-indexed
            (fn [i x]
              (list
                (om/build
                  single-work
                  {:paveikslas x
                   :judinimo-c judinimo-c
                   :pakeistas (pakeistas? ref-works x)
                   :judinamas (= judinamas (:id x))})
                (om/build
                  space-mark
                  {:position i :judinimo-c judinimo-c :tag tag})
                ))
            works)
          )))))

;(defn event-target-value [e] (.. e -target -value))

(defn works-editor [tag owner]
  (reify
    om/IWillMount
    (will-mount [_]
      (rc/-update-local! resource))
    om/IRender
    (render [_]
      (if tag
        (om/build works-view tag)
        (dom/ul
          (dom/li
            (om/build
              internal-link
              {:text "Redaguoti pirmą kolekciją" :sub-path "/paveikslai/1"}))
          (dom/li
            (om/build
              internal-link
              {:text "Redaguoti antrą kolekciją" :sub-path "/paveikslai/2"}))
          )))))

(defn works-changes-command [_ owner]
  (reify
    om/IRender
    (render [_]
      (let [nu (om/observe owner (cursors/get-works-cursor))
            old (om/observe owner (cursors/get-ref-works-cursor))
            changed?
            (not-empty
              (->>
                (map #(if (:delete %) % (dissoc % :delete)) nu)
                (rc/get-changed old)))
            pending?
            (some
              #(= :pending (:state %))
              (vals (om/observe owner (cursors/get-http-pulse-cursor))))
            issaugoti #(rc/-persist-changes! resource)
            atstatyti #(om/update! (cursors/get-works-cursor) old)]
        (dom/div
          {:id "pakeitimu-pultas"}
          (cond
            pending? "Vykdoma užklausa(-os)."
            changed? (dom/span
                       "Yra neišsaugotų pakeitimų. "
                       (dom/a {:href "#" :on-click issaugoti}
                              "Išsaugoti pakeitimus ")
                       "arba "
                       (dom/a {:href "#" :on-click atstatyti}
                              "atstatyti pakeitimus")
                       ".")
            true "Nėra pakeitimų."
            ))))))

