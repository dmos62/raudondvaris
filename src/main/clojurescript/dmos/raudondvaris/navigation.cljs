(ns dmos.raudondvaris.navigation
  (:require [om.core :as om :include-macros true]
            [om-tools.dom :as dom :include-macros true]
            [dmos.raudondvaris.route-match :refer [match-uri]]
            [dmos.raudondvaris.cursors :refer [get-token-info-cursor]]
            [dmos.raudondvaris.trace :refer [TRACE]]
            [goog.events]
            [goog.history.Html5History]
            [goog.history.EventType]
            [goog.Uri]))

(defn match-token [pattern token]
  (match-uri pattern token))

(def app-prefix (:prefix @(get-token-info-cursor)))

(defn app-path [sub-path] (str app-prefix sub-path))

(defn set-app-token! [path]
  (let [token (subs path (count app-prefix))]
    (om/update! (get-token-info-cursor) :token token)
    ))

(defonce history (doto
                   (goog.history.Html5History.)
                   (.setUseFragment false)
                   (.setEnabled true)
                   (.setPathPrefix "")))

(defn set-token! [token] (.setToken history token))

(defn handle-navigate! [e]
  (when (.-isNavigation e) 
    (set-app-token! (.-token e))))

(goog.events/listen history goog.history.EventType/NAVIGATE handle-navigate!)

(defn navigate-in-app! [e]
  (.preventDefault e)
  (let [path (.getPath (goog.Uri. (.. e -target -href)))]
    (.preventDefault e)
    (set-app-token! path)
    (set-token! path)))

(defn internal-link [{:keys [text sub-path]} owner]
  (reify
    om/IRender
    (render [_]
      (let [path (app-path sub-path)]
        (dom/span
          (str text ": ")
          (dom/a {:href path :on-click navigate-in-app!} path))))))
