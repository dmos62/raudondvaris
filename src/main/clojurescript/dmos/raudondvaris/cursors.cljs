(ns dmos.raudondvaris.cursors
  (:require [om.core :as om :include-macros true]))

(defonce state
  (atom {
         :paveikslai []
         :ref-paveikslai []
         :statics []
         :ref-statics []
         :token-info {:prefix "/a"}
         :http-pulse
         {
         ;"06" {:state :pending, :timestamp "2015-10-02T12:40:0.530Z"}
         ;"07" {:state :success, :timestamp "2015-11-02T12:40:24.530Z"}
         ;"08" {:state :failure, :timestamp "2015-12-02T12:40:24.530Z"}
         ;"09" {:state :success, :timestamp "2015-13-02T12:40:24.530Z"}
         ;"010" {:state :pending, :timestamp "2015-14-02T12:40:0.530Z"}
          }
         }))

; Kazkodel reikia naudoti ref-cursor o ne root-cursor
(defn get-main-cursor [] (om/ref-cursor (om/root-cursor state)))

(defn ref-cursor [sub-key]
  (om/ref-cursor (get (get-main-cursor) sub-key)))

(defn get-token-info-cursor [] (ref-cursor :token-info))

(defn get-works-cursor [] (ref-cursor :paveikslai))

(defn get-ref-works-cursor [] (ref-cursor :ref-paveikslai))

(defn get-statics-cursor [] (ref-cursor :statics)) 

(defn get-ref-statics-cursor [] (ref-cursor :ref-statics)) 

(defn get-http-pulse-cursor [] (ref-cursor :http-pulse))
