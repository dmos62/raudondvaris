(ns dmos.raudondvaris.admin
  (:require [om.core :as om :include-macros true]
            [om-tools.dom :as dom :include-macros true]
            [cljs.core.async
             :refer [close! take! chan put! <! >! to-chan]
             :as async]
            [clojure.string :as str]
            [dmos.raudondvaris.trace :refer [TRACE]]
            [dmos.raudondvaris.http-client :refer [<req]]
            [dmos.raudondvaris.navigation
             :refer [navigate-in-app! set-app-token! match-token
                     internal-link]]
            [dmos.raudondvaris.cursors :as cursors]
            [dmos.raudondvaris.works-editor
             :refer [works-editor works-changes-command]]
            [goog.events]
            [goog.events.EventType]
           ;[uzraktas.admin.testing-interface :refer [testing-interface]]
           ;[uzraktas.admin.works-editor :refer [works-editor]]
           ;[uzraktas.admin.posts-editor :refer [posts-editor]]
           ;[uzraktas.admin.statics-editor :refer [statics-editor]]
            )
  (:require-macros [cljs.core.async.macros :refer [go go-loop]]))

(enable-console-print!)

(defn resources-index-view [app owner]
  (reify
    om/IRender
    (render [_]
      (dom/ul
        {:id "turinys"}
        (dom/li
          (om/build
            internal-link
            {:text "Redaguoti paveikslus" :sub-path "/paveikslai"}))
        ))))

(defn breadtrail [app owner] 
  (reify 
    om/IRender
    (render [_]
      (let [token-info (om/observe owner (cursors/get-token-info-cursor))
            trail (->>
                    (-> (str (:prefix token-info) (:token token-info))
                        (str/split (re-pattern "/")))
                    (remove empty?))
            crumbs (loop [crumbs [(dom/span "/")]
                          trail-item (first trail)
                          trail (rest trail)
                          href-so-far (str "/" trail-item)]
                     (if-not trail-item
                       crumbs
                       (recur (conj crumbs
                                    (dom/span
                                      (dom/a {:href href-so-far
                                              :on-click navigate-in-app!}
                                             trail-item)
                                      (when-not (empty? trail) "/")))
                              (first trail)
                              (rest trail)
                              (str href-so-far "/" (first trail)))))]
        (dom/div
          {:id "bread"}
          "Duonos trupinėliai: " crumbs)))))

(defn admin-router [token-info]
  (condp match-token
    (:token token-info)
    "paveikslai/:?tag" :>>
    #(list (om/build works-changes-command nil)
           (dom/div {:id "turinys"} (om/build works-editor (:?tag %))))
    ;"posts/:?path" :>> #(om/build posts-editor (:?path %))
    ;"statics/:?path" :>> #(om/build statics-editor (:?path %))
    ;"test" (om/build testing-interface app)
    "/" (om/build resources-index-view nil)
    (om/build (fn [_ _] (om/component (dom/h1 "404"))) nil)
    ))

(defn http-pulse [app owner]
  (reify
    om/IRender
    (render [_]
      (let [requests
            (->> (om/observe owner (cursors/get-http-pulse-cursor))
                 vals
                 (sort-by :timestamp))]
        (dom/div
          {:id "http-pulse"}
          (map 
            (fn [r]
              (dom/figure
                {:class (str (name (:state r))
                             (when (:old r) " old"))}))
            requests)
          )))))

(defn root-admin-view [_ owner]
  (reify
    om/IRender
    (render [_]
      (let [token-info (om/observe owner (cursors/get-token-info-cursor))]
        (dom/div
          {:id "root"}
          (om/build http-pulse nil)
          (om/build breadtrail nil)
          ; cia turinys nera isskiriamas i savo heriarchine saka, nes tada admin routeris turi daugiau laisves
          (admin-router token-info)
          )))))

(defn main []
  (set-app-token! (.. js/window -location -pathname))
  (om/root
    root-admin-view
    cursors/state
    {:target (.-body js/document)}))

(goog.events/listen
  js/document
  goog.events.EventType/DOMCONTENTLOADED
  main)

(add-watch
  cursors/state :key
  (fn [_ _ o n]
    (let [o (:paveikslai o)
          n (:paveikslai n)]
      (when-not (= o n)
        ))))
