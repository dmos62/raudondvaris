(ns dmos.raudondvaris.utils
  (:import [goog.ui IdGenerator]
           [goog.date UtcDateTime]))


(defn new-uid [] (.getNextUniqueId (.getInstance IdGenerator)))

;(defn iso-date-string [] (.toIsoString (UtcDateTime.) true))

(defn iso-date-string [] (.toISOString (new js/Date)))

(defn parse-iso-date [iso-str] (.parse js/Date iso-str))
