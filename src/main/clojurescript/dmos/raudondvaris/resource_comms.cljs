(ns dmos.raudondvaris.resource-comms
  (:require [cljs.reader]
            [clojure.set :as set]
            [om.core :as om :include-macros true]
            [dmos.raudondvaris.trace :refer [TRACE]]
            [dmos.raudondvaris.http-client :as http-client]
            [cljs.core.async
             :refer [map< close! take! chan put! <! >! to-chan timeout]
             :as async]
            [cognitect.transit :as transit]
            [dmos.raudondvaris.functor :refer [fmap]]
            [dmos.raudondvaris.utils :as utils]
            [dmos.raudondvaris.cursors :as cursors])
  (:require-macros [cljs.core.async.macros :refer [go go-loop]]))

(defprotocol IEditor
  (update-local! [this])
  ;(update-local-by-name! [this neim])
  (persist! [this x])
  (persist-changes! [this])
  (insert-new! [this props]))

(defprotocol IResource
  (form-new [this props]))

; (defrecord Resource [cursor ref-cursor url make-default-new]
;   IResource
;   (form-new [_ props] (merge (make-default-new) props)))

(defrecord Resource [cursor ref-cursor url])

(declare -update-local! -persist-single! -persist-changes! -insert-new!)
         ;-update-local-by-name!)

(deftype Editor [^Resource rsrc]
  IEditor
  (update-local! [_] (-update-local! rsrc))
  ;(update-local-by-name! [_ neim] (-update-local-by-name! rsrc neim))
  (persist! [_ x] (-persist-single! rsrc x))
  (persist-changes! [_] (-persist-changes! rsrc))
  (insert-new! [_ props] (-insert-new! rsrc props)))

; ===========================================================================

;(defn <edn-req [& {:as hm}]
;  (->>
;    (assoc-in hm [:headers "Accept"] "application/edn")
;    (reduce into []) ; atsargiai su situ jeigu naudociau (), k ir v apsiverstu
;    (apply http-client/<req)))

(defn <req [{:as hm}]
  (let [uid (utils/new-uid)
        timestamp (utils/iso-date-string)
        cursor (cursors/get-http-pulse-cursor)]
    (->
      (assoc
        hm
        :before-f
        #(om/update! cursor [uid] {:state :pending :timestamp timestamp})
        :after-f
        (fn [r]
          (om/transact!
            cursor [uid]
            #(assoc % :state (if (< (:status r) 300) :success :failure)))
         ;(go (<! (timeout 5000))
         ;    (om/update! cursor [uid :old] true)
         ;    #_(om/transact! cursor #(dissoc % uid)))
          ))
      http-client/<req)))

(defn keys-to-keywords [hm]
  (zipmap (map keyword (keys hm)) (vals hm)))

(defn recursive-keywordifier [x]
  (cond
    (map? x) (->> (keys-to-keywords x) (fmap recursive-keywordifier))
    (coll? x) (fmap recursive-keywordifier x)
    true x))

(defn read-json [js]
  (let [edn (transit/read (transit/reader :json) js)]
    (recursive-keywordifier edn)))

(def transit-writer (transit/writer :json-verbose))

(defn write-json [x]
  (->>
    (clj->js x)
    (.stringify js/JSON)))

(defn read-body [resp] (-> resp :body read-json))

(defn <json-req [& {:as hm}]
  (->
    (assoc-in hm [:headers "Accept"] "application/json")
    <req))

; ===========================================================================

(defn <download [rsrc]
  (go
    (when-let [r (<! (<json-req :method "GET" :url (:url rsrc)))]
      (-> r read-body vec))))

(defn replace-start-if-matches [in a b]
  (if (and (some? in) (.startsWith in a))
    (str b (subs in (count a)))
    in))

(defn hack-painting [p] ; nes devserveris eilini karta bugina
  (update p :image
          #(replace-start-if-matches
             %
             "http://0.0.0.0:"
             "http://localhost:")))

(defn -update-local! [rsrc]
  (go
    (let [ps (<! (<download rsrc))
          ps (mapv hack-painting ps)]
      (om/update! (:ref-cursor rsrc) ps)
      (when (empty? @(:cursor rsrc))
        (om/update! (:cursor rsrc) ps)
        ))))

; (defn <download-single [rsrc neim]
;   (go
;     (when-let [r (<! (<req :method "GET"
;                           :url (str (:url rsrc) "/name/" neim)
;                           :headers {"Content-Type" "application/edn"}))]
;       (use-key-as-uid (cljs.reader/read-string (:body r))))))

; (defn -update-local-by-name! [rsrc neim]
;   (go
;     (let [x (<! (<download-single rsrc neim))]
;       (om/update! (:ref-cursor rsrc) ps)
;       (when (empty? @(:cursor rsrc))
;         (om/update! (:cursor rsrc) ps)
;         ))))

; ===========================================================================

(defn remove-vector-item [v i]
  (vec (concat (subvec v 0 i) (subvec v (inc i)))))

(defn first-index [pred col]
  (first (keep-indexed 
           #(when (pred %2) %1) col)))

(defn index-of-uid [v uid]
  (first-index #(= (:id %) uid) v))

(defn handle-deleted! [rsrc p]
  (let [wc (:cursor rsrc)
        rc (:ref-cursor rsrc)
        r-i (index-of-uid @rc (:id p))
        w-i (index-of-uid @wc (:id p))] 
    (om/transact! rc #(remove-vector-item % r-i))
    (om/transact! wc #(remove-vector-item % w-i))))

(defn update-by-index! [c i p]
  (if (= i :append)
    (om/transact! c #(conj % p))
    (om/update! c [i] p)))

(defn handle-updated! [rsrc p]
  (let [p (hack-painting p)
        wc (:cursor rsrc)
        rc (:ref-cursor rsrc)
        r-i (or (index-of-uid @rc (:id p)) :append)
        w-i (or (index-of-uid @wc (:id p)) :append)]
    (update-by-index! rc r-i p)
    (update-by-index! wc w-i p)))

; ===========================================================================

(defn <get-upload-url [cb-url]
  (go
    (:body
      (<! (<req
            {:method "POST" :url "/a/upload-url" :body cb-url})))))

; (defn images-url [rsrc ds-key] (str (:url rsrc) "/images/" ds-key))

; (defn <upload-images! [rsrc image-files-to-upload ds-key]
;   (go
;     (when (and (not-empty image-files-to-upload) ds-key)
;       (let [form (js/FormData.)
;             url (<! (<get-upload-url (images-url rsrc ds-key)))]
;         (doseq [[n file] image-files-to-upload]
;           (.append form n file))
;         (<! (<req :url url :method "POST" :body form))))))

(defn <upload-images! [url image-names-files]
  (go
    (when (not-empty image-names-files)
      (let [form (js/FormData.)
            url (<! (<get-upload-url url))]
        (doseq [[n file] image-names-files]
          (.append form n file))
        (<! (<json-req :url url :method "POST" :body form))))))

(defn <put-json! [rsrc edn]
  (go
    (let [url (str (:url rsrc) "/" (:id edn))]
      (->
        (<! (<json-req :url url :method "PUT" :body (write-json edn)))
        read-body
        ))))

; (defn <delete-images! [rsrc image-ids-to-delete ds-key]
;   (go
;     (when (and (not-empty image-ids-to-delete) ds-key)
;       (let [url (images-url rsrc ds-key)]
;         (<! (<req :url url :method "DELETE"
;                   :body (pr-str image-ids-to-delete)))))))

; (defn <chs-to-col [c & cs]
;   (go 
;     (let [c (if (empty? cs) c (async/merge (cons c cs)))]
;       (loop [rs []]
;         (if-let [r (<! c)]
;           (recur (conj rs r))
;           rs
;           )))))

(defn <delete-entity! [rsrc x]
  (go
    (let [r (<! (<json-req
                  :method "DELETE" :url (str (:url rsrc) "/" (x :id))))]
      (when (= 200 (:status r))
        ;(->
          (read-body r) ; turi grazinti {:key "asd" :gone true}
          ;(update :id #(js/parseInt %)))
        ))))

(defn -persist-single! [rsrc x]
  (go
    (when-let [r (<! (if (:delete x)
                       (<delete-entity! rsrc x)
                       (<put-json! rsrc x)))]
      (if (:gone r)
        (handle-deleted! rsrc r)
        (handle-updated! rsrc r))
      )))

(defn get-changed 
  ([rsrc]
   (get-changed @(:ref-cursor rsrc) @(:cursor rsrc)))
  ([old nu]
   (seq (set/difference (set nu) (set old)))))

(defn -persist-changes! [rsrc]
  (->>
    (get-changed rsrc)
    (map #(-persist-single! rsrc %))
    dorun
    ))

 ;(let [ps (get-changed rsrc)
 ;      cs (map
 ;           #(if (:delete %)
 ;              (<delete-entity! rsrc %)
 ;              (<json-edn! rsrc %))
 ;           ps)
 ;      c (async/merge cs)]
 ;  (go-loop
 ;    []
 ;    (when-let [x (<! c)]
 ;      (if (:gone x)
 ;        (handle-deleted! rsrc x)
 ;        (handle-updated! rsrc x))
 ;      (recur))))

; ===========================================================================

(defn -insert-new! [rsrc props]
  (let [n (form-new rsrc props)]
    (om/transact! (:cursor rsrc) #(conj % n))))
