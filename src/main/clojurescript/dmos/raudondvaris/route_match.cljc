(ns dmos.raudondvaris.route-match
  (:require [clojure.string :as str]
            [dmos.raudondvaris.trace :refer [TRACE]]))

(defn variable? [in]
  (cond
    (keyword? in) true
    (and (string? in) (= \: (first in))) true
    ))

(defn ensure-is-keyword [in]
  (cond
    (keyword? in) in
    (string? in) (keyword (if (= (first in) \:)
                            (subs in 1)
                            in))))

(defn variable-info [v]
  (when (variable? v)
    (let [type-bits (set (take 2 (name v)))]
      {:optional? (contains? type-bits \?)
       :multi-element? (contains? type-bits \*)})))

(defn apply-bindings-to-list [bindings output]
  (map #(if (variable? %) (get bindings %) %) output))

(defn match 
  ([input pattern] (match input pattern {}))
  ([input pattern bindings]
   ; input ir pattern bus jau parsinti i vectorius
   ; variable can only span one element
   (if (and (empty? input) (empty? pattern)) 
     bindings
     (let [input-el (first input)
           pattern-el (first pattern)
           variable (variable-info pattern-el)]
       (cond
         (:optional? variable)
         (if (:multi-element? variable)
           (assoc bindings pattern-el input)
           (recur (rest input) (rest pattern)
                  (assoc bindings pattern-el input-el)))
         (or (not input-el) (not pattern-el)) nil
         (some? variable)
         (if (:multi-element? variable)
           (assoc bindings pattern-el input)
           (recur (rest input) (rest pattern)
                  (assoc bindings pattern-el input-el)))
         (= input-el pattern-el)
         (recur (rest input) (rest pattern) bindings)
         ; no match returns nil
         )))))

(defn parse-uri-to-vector [uri]
  (when uri
    (if (coll? uri)
      uri
      (->>
        (str/split uri (re-pattern "/"))
        (remove empty?)))))

(defn keywordize-pattern [pat]
  (map #(if (variable? %) (ensure-is-keyword %) %) pat))

(defn parse-uri-to-pattern [uri]
  (-> (parse-uri-to-vector uri)
      keywordize-pattern))

(defn get-pattern [rule] (parse-uri-to-pattern (first rule)))
(defn get-output [rule] (last rule))

(defn match-against-rules [rules input]
  (let [input (if (sequential? input) ; if sequential means it's parsed
                input
                (parse-uri-to-pattern input))
        rule (first rules)
        pattern (get-pattern rule)
        output (get-output rule)]
    (when-not (nil? rule)
      (if-let [bindings (match input pattern)]
        (apply-bindings-to-list bindings output)
        (recur (rest rules) input)
        ))))

(defn match-uri [pattern uri]
  (match (parse-uri-to-vector uri) (parse-uri-to-pattern pattern)))
