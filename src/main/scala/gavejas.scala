package dmos.raudondvaris

import dmos.gae
import gae.RichEntity._
import gae.Datastore, Datastore.{Filter, Query}

object Gavejas {

  def paveikslai():List[Paveikslas] = {
    val q = 
      Query(Kindai.paveikslas)
        //.setFilter(Query.FilterOperator.EQUAL.of("category", kat))
    Datastore.gaukDaug[Paveikslas](q)
  }

  def paveikslaiSuTagu(tag:String):List[Paveikslas] = {
    val q = 
      Query(Kindai.paveikslas)
        .setFilter(Filter.equal("tag", tag))
    Datastore.gaukDaug[Paveikslas](q)
  }
}
