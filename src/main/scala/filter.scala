package dmos.raudondvaris.filter

import unfiltered.request._
import unfiltered.response._

import dmos.raudondvaris._
import dmos.gae
//import com.google.appengine.{api => aeapi}

class Entry extends unfiltered.filter.Plan {
  def intent = {
    case GET(Path("/")) => Demo.apie() // geriau redirect i /apie
    case GET(Path("/apie")) => Demo.apie()
    case GET(Path("/1")) =>
      Views.portfolio(Gavejas.paveikslaiSuTagu("1").map(_.juodas))
    case GET(Path("/2")) =>
      Views.portfolio(Gavejas.paveikslaiSuTagu("2").map(_.juodas))
  }
}

object Demo {
  def apie() =
    Views.apie(List(""))
}

object Views {
  import gae.Images

  private def base(body:scala.xml.NodeSeq) =
    Html5(
      <html>
        <head>
          <title></title>
          <link href="/css/main.css"
            rel="stylesheet" type="text/css" media="all"/>
        </head>
        <body>
            <header>
              <h1><a href="/">stasysart</a></h1>
              <nav>
                <a href="/apie">apie dailininką</a>
                <a class="active" href="/1">paveikslai 1</a>
                <a href="/2">paveikslai 2</a>
              </nav>
            </header>
            <section>
              {body}
            </section>
        </body>
      </html>)

  def apie(stulpeliai:List[String]):Html5 =
    base(
      stulpeliai
        .map( vienas =>
            <div class="stulpelis">
              {vienas.split("\n").map(p => <p>{p}</p>)}
            </div>
          ))

  def portfolio(paveikslai:List[JuodasPaveikslas]):Html5 =
    base(
      <ul id="portfolio">
        {paveikslai
          .map( p =>
          <li>
            <img src={
              val badStart = "http://0.0.0.0:8080/"
              if (p.image.startsWith(badStart))
                "http://localhost:8080/" +
                p.image.drop(badStart.length)
              else p.image
            }/>
          </li>)}
      </ul>)
}
