package dmos.raudondvaris

import dmos.gae
import gae.{
  Objektas, JuodasObjektas, Juodinamas, Kindas, Images}
import gae.Blobstore.BlobKey
import gae.Datastore.{Key, Entity}

/*
class Tekstas(
  val name:String,
  val text:String
)
*/

class Paveikslas(
  val image:BlobKey,
  val key:Key,
  val weight:Int,
  val tag:String)
extends Objektas with Juodinamas[JuodasPaveikslas] {

  val kind = Kindai.paveikslas

  lazy val blobs = Set(image)

  lazy val juodas =
    JuodasPaveikslas(
      image = Images.url(image), id = id, tag = tag, weight = weight)

  lazy val id = key.getId
}

object Paveikslas {
  import gae.{EntityFrom, ObjektasFrom}
  import gae.RichEntity._

  implicit val iEntity = EntityFrom[Paveikslas] { pav =>
    val e = new Entity(pav.key)
    e.setProperty("image", pav.image)
    e.setProperty("tag", pav.tag)
    e.setProperty("weight", pav.weight)
    e
  }

  implicit val iPaveiksla = ObjektasFrom[Paveikslas] { ent =>
    new Paveikslas(
      image = ent.get[BlobKey]("image"),
      key = ent.getKey,
      tag = ent.get[String]("tag"),
      weight = ent.get[Long]("weight").toInt)
  }

  import play.api.libs.json.{Json => PlayJson, _}
  import play.api.libs.functional.syntax._

  implicit def pWrites(implicit w:Writes[JuodasPaveikslas]) =
    new Writes[Paveikslas] {
      def writes(p:Paveikslas) = w.writes(p.juodas)
    }

  implicit def pReads(implicit r:Reads[JuodasPaveikslas]):Reads[Paveikslas] =
    r.map(_.svarus)
}

case class JuodasPaveikslas(
  id:Long, tag:String, weight:Int, image:String)
extends JuodasObjektas[Paveikslas] {
  import gae.Datastore
  val kind = Kindai.paveikslas
  lazy val key = Datastore.idToKey(kind, id)
  lazy val issaugotas = Datastore.gauk[Paveikslas](key)
  lazy val svarus = 
    new Paveikslas(
      image = issaugotas.image, key = key, tag = tag, weight = weight)
}

object JuodasPaveikslas {

  import play.api.libs.json.{Json => PlayJson, _}
  import play.api.libs.functional.syntax._

  implicit val jPFormat: Format[JuodasPaveikslas] = (
    (JsPath \ "id").format[Long] and
    (JsPath \ "tag").format[String] and
    (JsPath \ "weight").format[Int] and
    (JsPath \ "image").format[String]
  )(JuodasPaveikslas.apply, unlift(JuodasPaveikslas.unapply))

}

object Kindai {
  val paveikslas = Kindas("paveikslas")
  val tekstas = Kindas("tekstas")
}
