package dmos.raudondvaris.admin

import dmos.raudondvaris._
import dmos.gae

// Unfiltered Plan on top of servlets
object Servlet {
  import javax.servlet.{Servlet, ServletConfig, ServletRequest, ServletResponse}
  import javax.servlet.http.{HttpServletRequest, HttpServletResponse}
  import unfiltered.request._
  import unfiltered.response._
  import unfiltered.Cycle
  import unfiltered.filter._

  trait InittedServlet extends Servlet {
    private var configVar: ServletConfig = _
    def init(config: ServletConfig) { configVar = config; }
    def config = configVar
    def getServletConfig = configVar
    val getServletInfo = ""
    def destroy { }
  }

  object Plan {
    type Intent = Cycle.Intent[HttpServletRequest,HttpServletResponse]
  }

  trait Plan extends InittedServlet {
    def intent: Plan.Intent
    def service(request: ServletRequest, response: ServletResponse) {
      (request, response) match {
        case (hreq: HttpServletRequest, hres: HttpServletResponse) =>
          val request = new RequestBinding(hreq)
          val response = new ResponseBinding(hres)
          val rf = intent(request)
          val res = rf(response)
          res.outputStream.close()
      }
    }
  }
}

class Admin extends Servlet.Plan {
  import unfiltered.request._
  import unfiltered.response._

  /*
  import unfiltered.request.Accepts.Accepting
  object Accepts {
    object Edn extends Accepting {
      val contentType = "application/edn"
      val ext = "edn"
    }
  }
  */

  import unfiltered.request.Accepts
  import gae.Blobstore

  import java.io.{Writer,OutputStreamWriter}

  import play.api.libs.json.{Json => PlayJson, Reads, Writes}

  case class JsonWriter[T](input:T)(implicit writes:Writes[T])
  extends ResponseWriter {
    def write(w: OutputStreamWriter) {
      w.write(PlayJson.stringify(PlayJson.toJson(input)))
    }
  }

  object ReadJson {
    def apply[T](r: HttpRequest[T]) =
      PlayJson.parse(Body.string(r))
  }

  case class Json[T](input:T)(implicit writes:Writes[T])
  extends ComposeResponse(JsonContent ~> JsonWriter(input))

  def notFound = Status(404) ~> Html(<h2>Pasimetei. Arba aš pasimečiau.</h2>)

  def intent = {
    case req @ POST(Path("/a/upload-url")) =>
      ResponseString(Blobstore.uploadUrl(Body.string(req)))
    //case POST(_) => ResponseString("Hello")
    case req @ Accepts.Json(_) => req match {
      case GET(Path("/a/paveikslai")) =>
        Json(Gavejas.paveikslai())
      case PUT(Path(Seg("a" :: "paveikslai" :: id :: Nil))) => 
        Json(Valgytojas.atnaujinkPaveiksla(ReadJson(req).as[Paveikslas]))
      case POST(Path(Seg("a" :: "paveikslai" :: tag :: weight :: Nil))) =>
        Json(Valgytojas.naujasPaveikslas(req.underlying, weight.toInt, tag))
      case DELETE(Path(Seg("a" :: "paveikslai" :: id :: Nil))) =>
        Json(Valgytojas.trintiPaveiksla(id.toLong))
      case _ => notFound
    }
    case GET(Path(Seg("a" :: _))) => htmlDocument
    case _ => notFound
  }

  private val htmlDocument =
    Html5(
      <html>
        <head>
          <title></title>
          <link href="/css/admin.css"
            rel="stylesheet" type="text/css" media="all"/>
          <script type="text/javascript" src="/js/main.js"></script>
        </head>
        <body>
        </body>
      </html>)
}

object Valgytojas {
  import gae.{Datastore, Blobstore}
  import javax.servlet.http.HttpServletRequest
  import play.api.libs.json.{Json => PlayJson}

  def atnaujinkPaveiksla(paveikslas:Paveikslas):Paveikslas =
    Datastore.issaugok(paveikslas)

  def naujasPaveikslas(req:HttpServletRequest, weight:Int, tag:String
    ):Paveikslas =
    Datastore.issaugok(
      new Paveikslas(
        image = Blobstore.uploads(req)("image"),
        key = Datastore.naujasKey(Kindai.paveikslas),
        tag = tag,
        weight = weight))

  def trintiPaveiksla(id:Long) = {
    val paveikslas = Datastore.gauk[Paveikslas](Kindai.paveikslas, id)
    Blobstore.baibai(paveikslas.blobs)
    Datastore.trink(paveikslas.key)
    PlayJson.obj( "id" -> id, "gone" -> true)
    //json"""{"id": $id, "gone": true}"""
    //Map("id" -> id, "gone" -> true)
  }
}
